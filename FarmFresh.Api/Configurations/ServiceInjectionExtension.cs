﻿using FarmFresh.Application.Products.Contracts;
using FarmFresh.Application.Products.Services;
using FarmFresh.Domain.Repositories;
using FarmFresh.Domain.Repositories.Products;
using FarmFresh.Infrastructure.Repositories;
using FarmFresh.Infrastructure.Repositories.Products;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceInjectionExtension
    {
        public static IServiceCollection AddServiceInjection(this IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductRepository, ProductRepository>();
            return services;
        }
    }
}