﻿using FarmFresh.Application.Responses.Dtos;
using FarmFresh.Application.Products.Contracts;
using FarmFresh.Application.Products.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using FarmFresh.Application.Responses.Exceptions;

namespace FarmFresh.Api.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductService _productService;

        public ProductController(ILogger<ProductController> logger, IProductService productService)
        {
            _logger = logger;
            _productService = productService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<List<ProductListDto>>> GetAll(int skip = 0, int count = 10)
        {
            try
            {
                var products = await _productService.GetProducts(skip, count);
                return Ok(products);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong getting all Product. Skip {skip} record, take {count} record. {Message}", skip, count, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, MessageResponseDto.Error500());
            }
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ProductDetailDto>> Get([Required] Guid? id)
        {
            try
            {
                var product = await _productService.GetProducts(id.Value);
                return Ok(product);
            }
            catch (NotFoundException)
            {
                return NotFound(MessageResponseDto.Error404());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong finding Product with id {id}. {Message}", id, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, MessageResponseDto.Error500());
            }
        }

        [HttpPost("Add")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ProductDetailDto>> Add(ProductAddDto product)
        {
            try
            {
                var products = await _productService.Add(product);
                return Ok(products);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Something went wrong adding Product {product}. {Message}", product, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, MessageResponseDto.Error500());
            }
        }

        [HttpPost("Update")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<ProductDetailDto>> Update(ProductUpdateDto product)
        {
            try
            {
                var products = await _productService.Update(product);
                return Ok(products);
            }
            catch (NotFoundException)
            {
                return NotFound(MessageResponseDto.Error404());
            }
            catch (Exception ex)
            {
                _logger.LogError("Something went wrong updating Product {product}. {Message}", product, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, MessageResponseDto.Error500());
            }
        }

        [HttpDelete("Delete")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ValidationProblemDetails), StatusCodes.Status400BadRequest)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status404NotFound)]
        [ProducesResponseType(typeof(MessageResponseDto), StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult> Delete([Required] Guid? id)
        {
            try
            {
                await _productService.Delete(id.Value);
                return Ok();
            }
            catch (NotFoundException)
            {
                return NotFound(MessageResponseDto.Error404());
            }
            catch (Exception ex)
            {
                _logger.LogError("Something went wrong deleting Product with id {id}. {Message}", id, ex.Message);
                return StatusCode(StatusCodes.Status500InternalServerError, MessageResponseDto.Error500());
            }
        }
    }
}
