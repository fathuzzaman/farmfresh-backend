This is the backend of `FarmFresh` project, a sample .Net Api.

## Solution
This app is made using .NET 5. The solution consists of multiple projects:
- `Api`: The presentation layer which hold the controllers to be used as api endpoint.
- `Application`: The business logic layer, also acting as the bridge between presentation layer and domain.
- `Domain`: The core of the system, along with entities and repository interface
- `Infrastructure`: the lowest layer that acted as the gateway to the outside of the system such as database, storage, and other api service. This layer consists of ORM implementation (e.g. Entity Framework) and the repository interface defined in domain layer.

## Running the app
- Update connection string in `appsettings.json` in Api project so that it points to a real path.
- Do a migration using dotnet ef tool: `dotnet ef database update -p .\FarmFresh.Infrastructure -s .\FarmFresh.Api`.
- Run the app
