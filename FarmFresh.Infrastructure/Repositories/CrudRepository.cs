﻿using FarmFresh.Domain.Entities;
using FarmFresh.Domain.Repositories;
using FarmFresh.Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmFresh.Infrastructure.Repositories
{
    public class CrudRepository<T, TKey> : ICrudRepository<T, TKey> where T: Entity<TKey>
    {
        private readonly FarmFreshDbContext _dbContext;
        private readonly DbSet<T> _entities;

        public CrudRepository(FarmFreshDbContext dbContext)
        {
            _dbContext = dbContext;
            _entities = dbContext.Set<T>();
        }

        public async Task<List<T>> FindAllAsync(int skip, int? count)
        {
                var result = _entities.Skip(skip);
                if (count != null) result = result.Take(count.Value);
                result = result.OrderBy(x => x.Id);
                return await result.ToListAsync();
        }

        public async Task<T> FindAsync(TKey id)
        {
            return await _entities.FindAsync(id);
        }

        public async Task<T> AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _entities.Add(entity);
            await _dbContext.SaveChangesAsync();
            
            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _entities.Update(entity);
            await _dbContext.SaveChangesAsync();

            return entity;
        }

        public async Task DeleteAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            _entities.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(TKey id)
        {
            var entity = await _entities.FindAsync(id);
            await DeleteAsync(entity);
        }
    }
}
