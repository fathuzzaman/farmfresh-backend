﻿using FarmFresh.Domain.Repositories.Products;
using FarmFresh.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmFresh.Infrastructure.Repositories.Products
{
    public class ProductRepository : CrudRepository<Product, Guid>, IProductRepository
    {
        public ProductRepository(FarmFreshDbContext dbContext) : base(dbContext)
        {
        }
    }
}
