﻿using FarmFresh.Domain.Repositories.Products;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmFresh.Infrastructure.Data
{
    public class FarmFreshDbContext : DbContext
    {
        public FarmFreshDbContext(DbContextOptions options) : base(options)
        {
        }

        DbSet<Product> Products { get; set; }
    }
}
