﻿using FarmFresh.Application.Products.Dtos;
using FarmFresh.Domain.Repositories.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmFresh.Application.Products.Contracts
{
    public interface IProductService
    {
        Task<List<ProductListDto>> GetProducts(int skip, int count);
        Task<ProductDetailDto> GetProducts(Guid id);
        Task<ProductDetailDto> Add(ProductAddDto productAddDto);
        Task<ProductDetailDto> Update(ProductUpdateDto product);
        Task Delete(Guid id);
    }
}
