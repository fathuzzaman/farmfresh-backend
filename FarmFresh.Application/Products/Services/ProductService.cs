﻿using AutoMapper;
using FarmFresh.Application.Products.Contracts;
using FarmFresh.Application.Products.Dtos;
using FarmFresh.Application.Responses.Exceptions;
using FarmFresh.Domain.Repositories.Products;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FarmFresh.Application.Products.Services
{
    public class ProductService : IProductService
    {
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public ProductService(IMapper mapper, IProductRepository productRepository)
        {
            _mapper = mapper;
            _productRepository = productRepository;
        }

        public async Task<List<ProductListDto>> GetProducts(int skip, int count = 10)
        {
            var result = await _productRepository.FindAllAsync(skip, count);
            return _mapper.Map<List<ProductListDto>>(result);
        }

        public async Task<ProductDetailDto> GetProducts(Guid id)
        {
            var result = await _productRepository.FindAsync(id);
            if (result == null) throw new NotFoundException();

            return _mapper.Map<ProductDetailDto>(result);
        }

        public async Task<ProductDetailDto> Add(ProductAddDto productAddDto)
        {
            var product = _mapper.Map<Product>(productAddDto);
            product = await _productRepository.AddAsync(product);
            return _mapper.Map<ProductDetailDto>(product);
        }

        public async Task<ProductDetailDto> Update(ProductUpdateDto productUpdateDto)
        {
            var existingProduct = await _productRepository.FindAsync(productUpdateDto.Id.Value);
            if (existingProduct == null) throw new NotFoundException();

            var product = _mapper.Map<Product>(productUpdateDto);
            product = await _productRepository.UpdateAsync(product);
            return _mapper.Map<ProductDetailDto>(product);
        }

        public async Task Delete(Guid id)
        {
            var existingProduct = await _productRepository.FindAsync(id);
            if (existingProduct == null) throw new NotFoundException();

            await _productRepository.DeleteAsync(id);
        }
    }
}
