﻿using System.ComponentModel.DataAnnotations;

namespace FarmFresh.Application.Products.Dtos
{
    public class ProductAddDto
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Uom { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string OriginCountry { get; set; }
    }
}
