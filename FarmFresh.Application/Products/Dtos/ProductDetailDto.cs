﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmFresh.Application.Products.Dtos
{
    public class ProductDetailDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Uom { get; set; }
        public string Description { get; set; }
        public string OriginCountry { get; set; }
    }
}
