﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FarmFresh.Application.Responses.Dtos
{
    public class MessageResponseDto
    {
        private const string default404Message = "Not found";
        private const string default500Message = "Something went wrong";

        public string Message { get; set; }
        public MessageResponseDto(string message)
        {
            Message = message;
        }

        public static MessageResponseDto Error404()
        {
            return new MessageResponseDto(default404Message);
        }

        public static MessageResponseDto Error500() {
            return new MessageResponseDto(default500Message);
        }

    }
}
