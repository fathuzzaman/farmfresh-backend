﻿using AutoMapper;
using FarmFresh.Application.Products.Dtos;
using FarmFresh.Domain.Repositories.Products;

namespace FarmFresh.Application.ObjectMapper
{
    public class AutoMapProfile : Profile
    {
        public AutoMapProfile()
        {
            CreateMap<Product, ProductListDto>();
            CreateMap<Product, ProductDetailDto>();
            CreateMap<ProductAddDto, Product>();
            CreateMap<ProductUpdateDto, Product>();
        }
    }
}
