﻿using System.ComponentModel.DataAnnotations;

namespace FarmFresh.Domain.Entities
{
    public class Entity<TKey>
    {
        [Key]
        public TKey Id { get; set; }
    }
}
