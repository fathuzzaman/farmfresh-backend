﻿using FarmFresh.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmFresh.Domain.Repositories.Products
{
    public class Product : Entity<Guid>
    {
        public string Name { get; set; }
        public string Uom { get; set; }
        public string Description { get; set; }
        public string OriginCountry { get; set; }
    }
}
