﻿using FarmFresh.Domain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FarmFresh.Domain.Repositories
{
    public interface ICrudRepository<T, TKey> where T : Entity<TKey>
    {
        Task<List<T>> FindAllAsync(int skip, int? count);
        Task<T> FindAsync(TKey id);
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task DeleteAsync(TKey id);
    }
}
