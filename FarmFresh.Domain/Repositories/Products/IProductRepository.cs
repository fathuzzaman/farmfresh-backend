﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FarmFresh.Domain.Repositories.Products
{
    public interface IProductRepository : ICrudRepository<Product, Guid>
    {
    }
}
